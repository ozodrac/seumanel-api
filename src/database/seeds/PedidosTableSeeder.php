<?php

use Illuminate\Database\Seeder;

class PedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        DB::table('pedidos')->insert([
            'data_compra' => date('Y-m-d H:i:s'),
            'nome_comprador' => "Comprador 1", 
            'valor_frete' => 3.00            
            ]);
        // Produtos do pedido 1
        DB::table('produtos_pedido')->insert(['pedido_id' => 1,'produto_id' => 3,'quantidade' => 4,'preco_venda' => DB::table('produtos')->where('id',3)->value(DB::RAW('preco * 4')) ]);
        DB::table('produtos_pedido')->insert(['pedido_id' => 1,'produto_id' => 5,'quantidade' => 3,'preco_venda' => DB::table('produtos')->where('id',5)->value(DB::RAW('preco * 3')) ]);
        DB::table('produtos_pedido')->insert(['pedido_id' => 1,'produto_id' => 6,'quantidade' => 2,'preco_venda' => DB::table('produtos')->where('id',6)->value(DB::RAW('preco * 2')) ]);
    }
}
