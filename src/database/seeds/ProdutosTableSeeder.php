<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert(['nome' => "Monofase", 'descricao' => 'Batata Frita tamanho P', 'Estoque' => 100, 'preco'=> 8.00]);
        DB::table('produtos')->insert(['nome' => "Bifase", 'descricao' => 'Batata Frita tamanho M', 'Estoque' => 100, 'preco'=> 16.00]);
        DB::table('produtos')->insert(['nome' => "Trifase", 'descricao' => 'Batata Frita tamanho G', 'Estoque' => 100, 'preco'=> 21.35]);
        DB::table('produtos')->insert(['nome' => "Coca-cola 300ml", 'descricao' => 'Coca-cola Lata', 'Estoque' => 100, 'preco'=> 4.00]);
        DB::table('produtos')->insert(['nome' => "PituCola 300ml", 'descricao' => 'Patrocina nois pitu <3', 'Estoque' => 100, 'preco'=> 3.00]);
        DB::table('produtos')->insert(['nome' => "Adicional de Bacon", 'descricao' => 'Porção extra de bacon 100g`', 'Estoque' => 100, 'preco'=> 5.00]);
        DB::table('produtos')->insert(['nome' => "Adicional de calabreza", 'descricao' => 'Porção extra de calabreza 100g`', 'Estoque' => 100, 'preco'=> 4.00]);
    }
}
