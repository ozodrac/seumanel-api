<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidos extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'pedidos';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // código identificador, data da compra, nome do comprador, estado (ex: novo, aprovado, entregue e cancelado), valor do frete e a lista de itens que foram vendidos - cada item possui: código do produto; quantidade; e o preço de venda
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->dateTime('data_compra')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('nome_comprador', 50);
            $table->enum('status', ['novo', 'aprovado','entregue','cancelado'])->nullable()->default('novo');
            $table->float('valor_frete')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->index("id", 'idx_pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}
