<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'produtos';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // código e terá as informações de nome, descrição, estoque, preço e mais alguns
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nome', 50);
            $table->text('descricao')->nullable();
            $table->integer('estoque')->unsigned()->nullable()->default(0);
            $table->float('preco')->unsigned()->nullable()->default(0);
            $table->text('caracteristicas')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index("id", 'idx_produtos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}
