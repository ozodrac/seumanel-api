<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosPedido extends Migration
{
    public $set_schema_table = 'produtos_pedido';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //lista de itens que foram vendidos - cada item possui: código do produto; quantidade; e o preço de venda
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pedido_id')->unsigned();
            $table->integer('produto_id')->unsigned();
            $table->integer('quantidade')->unsigned()->nullable();
            $table->float('preco_venda')->unsigned()->nullable();
            $table->index([ "pedido_id", "produto_id"], 'idx_produtos_pedido');
        });

        Schema::table($this->set_schema_table, function ($table) {
            $table->foreign( 'produto_id', 'fk_produtos_pedido_id_produto')
                ->references('id')->on('produtos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign( 'pedido_id', 'fk_produtos_pedido_id_pedido')
                ->references('id')->on('pedidos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}