<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Illuminate\Http\Request;


$router->get('/key', function () use ($router) {
    return str_random(32);
});
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/'], function ($router) {
    $router->get('/ticket', function (Request $request) use ($router) {

        $totalVendido = App\Pedido::addSelect([DB::RAW('sum(produtos_pedido.preco_venda) AS total')])
        ->join('produtos_pedido','pedidos.id','=','pedido_id')
        ->where('pedidos.status','<>','cancelado')
        ->get()->first();

        $pedidosPeriodo =  App\Pedido::addSelect([ DB::RAW("(COUNT(pedidos.id) ) AS total") ])
        ->where('pedidos.status','<>','cancelado')
        ->whereBetween('pedidos.data_compra',["$request->inicio", "$request->fim"])
        ->get()->first();

        if($totalVendido->total > 0){
            if($pedidosPeriodo->total > 0){
                return "Total de Faturamento: R$ {$totalVendido->total} <br> 
                Ticket Medio: R$: $totalVendido->total/$pedidosPeriodo->total <br>
                Total de Vendas no periodo: $pedidosPeriodo->total ";
            }else{
                return 0;
            }
        }
        return [$totalVendido , $pedidosPeriodo ];
    });

    $router->group(['prefix' => 'produtos'],function($router){
        $router->get('/', 'ProdutoController@listaProdutos');
        $router->get('/{id}', 'ProdutoController@show');
        $router->post('/', 'ProdutoController@store');
        $router->put('/{id}', 'ProdutoController@update');
        $router->delete('/{id}', 'ProdutoController@destroy');
    });

    $router->group(['prefix' => 'pedidos'],function($router){
        $router->get('/', 'PedidoController@listaPedidos');
        $router->get('/{id}', 'PedidoController@show');
        $router->post('/', 'PedidoController@store');
        $router->put('/{id}', 'PedidoController@update');
        $router->delete('/{id}', 'PedidoController@destroy');
    });
});
