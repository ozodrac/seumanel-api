<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosPedido extends Model
{
    protected $table = 'produtos_pedido';

    function produtosPedido()
    {
        return $this->belongsToMany(['App\Produto', 'App\Pedido']);
    }

}
