<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Pedido extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['nome_comprador', 'status', 'valor_frete'];
    protected $dates = ['deleted_at'];
    protected $table = 'pedidos';

    // protected static function boot()
    // {
    //     parent::boot();
    //     static::deleting(function ($pedido) {
    //         foreach ($pedido->produtos()->get() as $produto) {
    //             $produto->delete();
    //         }
    //     });
    // }

    public function produtos()
    {
        return $this->hasMany('App\ProdutosPedido');
    }
    public function getProdutosDoPedido($id)
    {
        return DB::table('produtos_pedido')
            ->join('produtos', 'produtos.id', '=', 'produto_id')
            ->where('pedido_id', '=', $id)
            // ->pluck('produtos.nome', 'produtos.id');
            ->get(['produtos_pedido.*','produtos.nome','produtos.estoque','produtos.descricao','produtos.preco','produtos.caracteristicas'])->toArray();
    }

}
