<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;

class ProdutoController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function listaProdutos()
    {
        $produtos = Produto::all();
        $responsecode = !empty($produtos)? 200 : 404;
        return response($produtos, $responsecode)->header('Content-type','application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produto = Produto::create($request->only( 'nome', 'descricao', 'estoque', 'preco', 'caracteristicas'));
        $responsecode = !empty($produtos)? 201 : 400;
        return response($produto, $responsecode)->header('Content-type','applicat  ion/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = Produto::findOrFail($id);
        return response($produto, 200)->header('Content-type','applicat ion/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old = Produto::findOrFail($id);
        $produto = Produto::findOrFail($id);

        $input = $request->only('nome', 'descricao', 'estoque', 'preco', 'caracteristicas');
        $produto->fill($input)->save();

        return response(['Produto Antes->'=> $old, 'Produto Depois ->'=> $produto], 200)->header('Content-type','applicat   ion/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);
        $produto->delete($id);
        return response($produto, 200)->header('Content-type','applicat  ion/json');
    }
}
