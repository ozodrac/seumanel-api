<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Pedido;
use App\ProdutosPedido;
use Illuminate\Support\Facades\DB;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listaPedidos()
    {
        $pedidos = Pedido::all();
        foreach($pedidos as $pedido){
            $pedido->produtos = $pedido->getProdutosDoPedido($pedido->id);
        }
        $responsecode = !empty($produtos)? 200 : 404;
        return response($pedidos, $responsecode)->header('Content-type','application/json');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->produtos);
        if (!empty($request->produtos)) {
            $pedido = Pedido::create($request->only( 'nome_comprador', 'valor_frete'));

            $mensagem = array();
            $produtosNaoEncontrados = array();
            $produtosSemEstoque = array();
            foreach ($request->produtos as $key => $prod_id) {
                $produto = Produto::find($prod_id);
                if(!$produto){
                    $mensagem[] = " Código de produto {$prod_id} inexistente";
                }else{
                    $produto = $produto->toArray();
                    if(!empty($request->quantidade[$key]) && intval($request->quantidade[$key])  ){

                        if( ($produto['estoque'] -$request->quantidade[$key]) >= 0){
                            
                            DB::table('produtos_pedido')
                            ->insert([
                                'pedido_id' => $pedido->id, 
                                'produto_id' => $prod_id,
                                'quantidade' => $request->quantidade[$key],
                                'preco_venda' => ($produto['preco'] * $request->quantidade[$key]),
                                ]);
                            DB::table('produtos')->where('id', '=', $prod_id)->update(['estoque' => ($produto['estoque'] - $request->quantidade[$key] ) ]);
                            $novoestoque = ($produto['estoque'] - $request->quantidade[$key] );
                            $mensagem[] = " Pedido de {$request->quantidade[$key]} x {$produto['nome']} realizado, restam  {$novoestoque} em estoque ";
                        }else{
                            $mensagem[] = " Quantidade solicitada ({$request->quantidade[$key]}) para o produto {$produto['nome']} ultrapassa o estoque atual ({$produto['estoque']}) ";
                        }
                    }else{
                        $mensagem[] = " Quantidade Vazia para o produto {$produto['nome']}";
                    }
                }
            }
            return response($mensagem, 200)->header('Content-type','applicat  ion/json');
        } else {
            return response('Pedido sem produtos', 400)->header('Content-type','applicat  ion/json');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = Pedido::findOrFail($id);
        $pedido->produtos = $pedido->getProdutosDoPedido($id);
        return response($pedido, 200)->header('Content-type','applicat ion/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old = Pedido::findOrFail($id);
        $old->produtos = $old->getProdutosDoPedido($id);

        $pedido = Pedido::findOrFail($id);
        $produtosPedidos = $pedido->getProdutosDoPedido($id);
        $input = $request->only('nome_comprador', 'valor_frete','status','quantidade');

        if($pedido->toArray()['status'] !='entregue'){
            
            switch ($request->status) {
                case 'cancelado':
                    foreach($produtosPedidos as $prod){
                        $produto = Produto::find($prod->produto_id);
                        DB::table('produtos')->where('id', '=', $produto->id)->update(['estoque' => ($produto->estoque + $prod->quantidade ), ]);
                    }
                    $pedido->fill($input)->save();
                    return response('Pedido cancelado', 200)->header('Content-type','applicat   ion/json');
                break;
                default:
                    
                    if (!empty($request->produtos)) {
                        $mensagem = array();
                        $produtosNaoEncontrados = array();
                        $produtosSemEstoque = array();
                        foreach ($request->produtos as $key => $prod_id) {
                            $produto = DB::table('produtos')->where('id', '=', $prod_id)->lockForUpdate()->get()->first();
                            if(!$produto){
                                $mensagem[] = " Código de produto {$prod_id} inexistente";
                            }else{
                                if(!empty($request->quantidade[$key]) && intval($request->quantidade[$key])  ){
                                    if( (($produto->estoque + $produtosPedidos[$key]->quantidade) - $request->quantidade[$key]) >= 0){
                                        DB::table('produtos')->where('id', '=', $prod_id)->update(['estoque' => ($produto->estoque + $produtosPedidos[$key]->quantidade ) ]);
                                        DB::table('produtos_pedido')->where('produto_id', '=', $prod_id)->where('pedido_id', '=', $id)->delete();
                                        
                                        DB::table('produtos_pedido')
                                        ->insert([
                                            'pedido_id' => $pedido->id, 
                                            'produto_id' => $prod_id,
                                            'quantidade' => $request->quantidade[$key],
                                            'preco_venda' => ($produto->preco * $request->quantidade[$key]),
                                            ]);
                                        DB::table('produtos')->where('id', '=', $prod_id)->update(['estoque' => ($produto->estoque - $request->quantidade[$key] ) ]);
                                        $novoestoque = ($produto->estoque - $request->quantidade[$key] );
                                        $mensagem[] = " Pedido de {$request->quantidade[$key]} x {$produto->nome} realizado, restam  {$novoestoque} em estoque ";
                                    }else{
                                        $mensagem[] = " Quantidade solicitada ({$request->quantidade[$key]}) para o produto {$produto->nome} ultrapassa o estoque atual ({$produto->estoque}) ";
                                    }
                                }else{
                                    $mensagem[] = " Quantidade Vazia para o produto {$produto->nome}";
                                }
                            }
                        }
                        $mensagem [] = "Pedido {$id} para o comprador {$pedido->nome_comprador} foi {$request->status} ";
                        $pedido->fill($input)->save();
                        return   response($mensagem, 200)->header('Content-type','applicat  ion/json');
                    } else {
                        return response('Pedido sem produtos', 400)->header('Content-type','applicat  ion/json');
                    }
                break;
            }
        }else{
            return response('Pedido já entregue, não é possivel editar', 400)->header('Content-type','applicat   ion/json');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido = Pedido::find($id);
        $pedido->delete($id);
        return response($pedido, 200)->header('Content-type','applicat  ion/json');
    }
}
